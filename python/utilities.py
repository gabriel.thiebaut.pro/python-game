def diffList(L1, L2):
    L = [x for x in L1 if not x in L2]

    return L


def addByComponents(L1, L2):
    return [L1[0] + L2[0], L1[1] + L2[1]]
