import pygame


class Fruit:
    def __init__(self, position, color):
        self.position = position
        self.color = color

    def drawFruit(self, casesSize, screen):
        rectangle = pygame.Rect(
            self.position[0]*casesSize, self.position[1]*casesSize, casesSize, casesSize)

        pygame.draw.rect(screen, self.color, rectangle, casesSize)
        pygame.display.flip()

    def getPosition(self):
        return self.position
