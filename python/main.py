import pygame
import sys
from pygame import draw
from pygame.constants import QUIT
from pygame.locals import *
from pygame.mixer import *
from Snake import *
from Fruit import *
from random import choice, randint
from utilities import *
from math import exp


def main():
    global screen, heigth, casesSize
    pygame.init()
    pygame.font.init()

    pygame.display.set_caption('GabySnake')
    pygame.display.set_icon(pygame.image.load('python/logo.png'))

    pygame.mixer.init()
    pygame.mixer.music.load('python/music.mp3')

    # screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    screen = pygame.display.set_mode((800, 500))
    screen.fill((0, 0, 0))

    score = 0
    cases = 20
    delay0 = 150
    delay = delay0
    lastMove = -delay
    lastDir = None
    offset = [[0, -1], [0, 1], [1, 0], [-1, 0]]

    font = pygame.font.SysFont("Carlito", 30)
    labelScoreStr = font.render("Score", 1, (255, 255, 255))
    labelDelayStr = font.render("Delay", 1, (255, 255, 255))
    screen.blit(labelScoreStr, (550, 0))
    screen.blit(labelDelayStr, (550, 200))

    displayStats(font, score, delay)

    _, heigth = pygame.display.get_surface().get_size()
    casesSize = int(heigth/cases)

    drawGrid(casesSize)

    startX, startY = randint(1, cases-2), randint(1, cases-2)

    s = Snake([[startX, startY], addByComponents(
        [startX, startY], choice(offset))], (0, 255, 0), (100, 255, 100))
    s.drawSnake(casesSize, screen)

    f = newFruit(cases, s.getPositions())

    run = True
    snakeGrowing = False
    dirAlreadyChanged = False
    startMusic = True

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        dir = getDirection()
        if dir != None and lastDir != [-dir[0], -dir[1]] and not dirAlreadyChanged:
            lastDir = dir
            dirAlreadyChanged = True

            if startMusic:
                pygame.mixer.music.play(loops=1000)
                startMusic = False

        if s.getHeadPosition() == f.getPosition():
            snakeGrowing = True

        if pygame.time.get_ticks() - lastMove > delay:
            moveSnake(s, lastDir, snakeGrowing)
            if snakeGrowing:
                f = newFruit(cases, s.getPositions())

                score += 1
                delay = delay0*exp(-0.15*score)

                if delay < 20:
                    delay = 20

                displayStats(font, score, delay)

            snakeGrowing = False
            lastMove = pygame.time.get_ticks()
            dirAlreadyChanged = False

        if s.hasLost(cases):
            run = False

    pygame.quit()


def drawGrid(casesSize):
    for x in range(0, heigth, casesSize):
        for y in range(0, heigth, casesSize):
            rectangle = pygame.Rect(x, y, casesSize, casesSize)
            pygame.draw.rect(screen, (255, 255, 255), rectangle, 1)
    pygame.display.flip()


def getDirection():
    keysPressed = pygame.key.get_pressed()
    keys = {pygame.K_z: [0, -1], pygame.K_s: [0, 1],
            pygame.K_d: [1, 0], pygame.K_q: [-1, 0]}

    for k in keys:
        if keysPressed[k]:
            return keys[k]

    return None


def moveSnake(s, dir, snakeGrowing):
    if dir != None:
        if not snakeGrowing:
            s.eraseSnake(casesSize, screen)

        s.move(dir, snakeGrowing)
        s.drawSnake(casesSize, screen)


def newFruit(cases, forbiddenCases):
    fullCases = [[x, y] for x in range(cases) for y in range(cases)]
    authorizedCases = diffList(fullCases, forbiddenCases)

    f = Fruit(choice(authorizedCases), (255, 0, 0))
    f.drawFruit(casesSize, screen)
    return f


def displayStats(font, score, delay):
    clear1 = pygame.Rect(550, 50, 300, 50)
    clear2 = pygame.Rect(550, 250, 300, 50)
    pygame.draw.rect(screen, (0, 0, 0), clear1, 300)
    pygame.draw.rect(screen, (0, 0, 0), clear2, 300)

    labelScoreInt = font.render(str(score), 1, (255, 255, 255))
    labelDelayInt = font.render(str(int(delay)) + ' ms', 1, (255, 255, 255))

    screen.blit(labelScoreInt, (550, 50))
    screen.blit(labelDelayInt, (550, 250))

    pygame.display.flip()


main()
