import pygame
from utilities import *


class Snake:
    def __init__(self, positions, colorHead, colorTail):
        self.positions = positions
        self.colorHead = colorHead
        self.colorTail = colorTail

    def move(self, dir, snakeGrowing):
        if snakeGrowing:
            tailPosition = self.positions[-1]
            print(tailPosition)

        for i in range(len(self.positions)-1, 0, -1):
            self.positions[i] = list(self.positions[i-1])

        if snakeGrowing:
            self.positions.append(tailPosition)

        self.positions[0] = addByComponents(self.positions[0], dir)

    def drawSnake(self, casesSize, screen):
        rectangleHead = pygame.Rect(
            self.positions[0][0]*casesSize, self.positions[0][1]*casesSize, casesSize, casesSize)

        rectangleSecond = pygame.Rect(
            self.positions[1][0]*casesSize, self.positions[1][1]*casesSize, casesSize, casesSize)

        pygame.draw.rect(screen, self.colorHead, rectangleHead, casesSize)
        pygame.draw.rect(screen, self.colorTail, rectangleSecond, casesSize)

        pygame.display.flip()

    def eraseSnake(self, casesSize, screen):
        rectangle = pygame.Rect(
            self.positions[-1][0]*casesSize, self.positions[-1][1]*casesSize, casesSize, casesSize)

        pygame.draw.rect(screen, (0, 0, 0), rectangle, casesSize)
        pygame.draw.rect(screen, (255, 255, 255), rectangle, 1)
        pygame.display.flip()

    def hasLost(self, cases):
        for i in range(1, len(self.positions)):
            print(self.positions)
            if self.positions[0] == self.positions[i]:
                return True

        return self.positions[0][0] > cases - 1 or self.positions[0][1] > cases - \
            1 or self.positions[0][0] < 0 or self.positions[0][1] < 0

    def getPositions(self):
        return self.positions

    def getHeadPosition(self):
        return self.positions[0]
